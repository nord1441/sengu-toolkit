#!/bin/bash

function executeEntryKeyMatched() {
	KEY="$1"
	cat - | \
	grep "$KEY" | sort -n | \
	cut --complement -d\  -f 1,2 | \
	xargs -I {} bash -c {}
}


function setSelection() {
	MACHINETYPE=$1
	SELECTION=$2
	DIR=$3

	cd $DIR
	rm -f selected.list
	cat lists/minimal.list >> selected.list
	if [ $SELECTION = "full" ]
	then
		cat lists/full.list >> selected.list
	fi
	if [ $MACHINETYPE = "real" ]
	then
		cat lists/real.list >> selected.list
	fi
}

MACHINETYPE="cloud"
SELECTION="minimal"
CRD="disabled"
export SETUP_ROOT=$(dirname $(realpath $0))

while getopts mfrcuC OPT
do
	case $OPT in
		m)	SELECTION="minimal"
			;;
		f)	SELECTION="full"
			;;
		r)	MACHINETYPE="real"
			;;
		c)	MACHINETYPE="cloud"
			;;
		u)	sudo apt-get update && \
			sudo apt-get upgrade -y && \
			sudo apt-get dist-upgrade -y
			;;
		C)	CRD="enabled"
			;;
		\?)	echo "usage: $0 [-mfrcuC]"
			exit
			;;
	esac
done
shift $((OPTIND - 1))

if [ $MACHINETYPE = "real" ]
then
	grep 'GRUB_CMDLINE_LINUX_DEFAULT=""' /etc/default/grub 2>&1 > /dev/null && sudo sed -i -e "s/GRUB_CMDLINE_LINUX_DEFAULT=\"\"/GRUB_CMDLINE_LINUX_DEFAULT=\"usbcore.autosuspend=-1\"/g" /etc/default/grub
	sudo update-grub
fi

cd $SETUP_ROOT
if [ $CRD = "enabled" ]
then
	cat lists/setup.list | executeEntryKeyMatched 'setupCrd'
fi
cat lists/setup.list | executeEntryKeyMatched 'addFile'
cat lists/setup.list | executeEntryKeyMatched 'addFwRules'
cat lists/setup.list | executeEntryKeyMatched 'disableService'
setSelection $MACHINETYPE $SELECTION .
cat lists/setup.list | executeEntryKeyMatched 'installApps'
cat lists/setup.list | executeEntryKeyMatched 'enableService'
cat lists/setup.list | executeEntryKeyMatched 'setupUser'
