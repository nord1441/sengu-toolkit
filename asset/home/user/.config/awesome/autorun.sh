#!/usr/bin/env bash

function run {
	if ! pgrep $1 ;
	then
		$@&
	fi
}

function mountGoogleDrive {
	TARGET="$HOME/GoogleDrive"
	mount | grep $TARGET > /dev/null
	if [ $? -ne 0 ]
	then
		/usr/bin/google-drive-ocamlfuse $TARGET &
	fi
}

run mountGoogleDrive
