#!/bin/python

import gkeepapi

class authenticator:
	def __init__(self):
		self.readAuthInfo()
		self.login()
	def readAuthInfo(self):
		with open('/usr/local/bin/authInfo') as info:
			self.username = info.readline()
			self.password = info.readline()
	def login(self):
		self.account = gkeepapi.Keep()
		self.target = self.account.login(self.username, self.password)
	def getAccount(self):
		return self.account
	def getTarget(self):
		return self.target
