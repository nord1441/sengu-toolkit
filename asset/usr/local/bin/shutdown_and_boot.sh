#!/bin/bash

sleepHours="$1"
sudo sh -c "echo 0 > /sys/class/rtc/rtc0/wakealarm"
sudo sh -c "echo `date '+%s' -d \"+ $sleepHours hours\"` > /sys/class/rtc/rtc0/wakealarm"
sudo shutdown -h now
