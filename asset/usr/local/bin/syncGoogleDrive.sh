#!/bin/bash

USER="$1"
DIR="$2"
unison /home/$USER/GoogleDrive/$DIR /home/$USER/$DIR -batch -copyonconflict -prefer /home/$USER/GoogleDrive/$DIR -fastcheck false
