#!/bin/python

import datetime
import sys
import glob
import getpass

today = datetime.datetime.now()
content = "\n# " + today.strftime('%Y-%m-%d') + "\n"
args = sys.argv
with open("/home/" + getpass.getuser() + "/" + args[1] + "/Journal/" + today.strftime('%Y-%m') + ".md", 'a') as f:
	f.writelines(content)


