#!/bin/bash

DEVICE="$1"
STATUS="`LANG=C nmcli device status | grep $DEVICE | awk '{print $3}'`"
if [ "$STATUS" == "disconnected" ]
then
	nmcli device connect "$DEVICE"
fi
