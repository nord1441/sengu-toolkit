#!/bin/bash

CRDSETUPCMD=""
CRDPIN=""
export SETUP_ROOT=$(dirname $(realpath $0))

while getopts C:P: OPT
do
	case $OPT in
		C)	CRDSETUPCMD="$OPTARG"
			;;
		P)	CRDPIN="$OPTARG"
			;;
		\?)	echo "usage: $0 [-CP]"
			exit
			;;
	esac
done
shift $((OPTIND - 1))

cd $SETUP_ROOT
rm -f crd-credential.txt
echo $CRDPIN >> crd-credential.txt
echo $CRDSETUPCMD >> crd-credential.txt
